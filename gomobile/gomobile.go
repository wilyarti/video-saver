package gomobile

import (
	"fmt"
	"os"
	"regexp"
	"time"

	"github.com/rylio/ytdl"
)

// callback
var jc JavaCallback

type JavaCallback interface {
	SendString(string)
}

func RegisterJavaCallback(c JavaCallback) {
	jc = c
}

func TestCall() {
	for i := 0; i < 100; i++ {
		time.Sleep(100 * time.Millisecond)
		jc.SendString(fmt.Sprintln("Counting... ", i))
	}
}
func Gogetvideo(url string, dir string, res string) {
	go Getvideo(url, dir, res)
}

func Getvideo(url string, dir string, res string) {
	vid, err := ytdl.GetVideoInfo(url)
	if err != nil {
		fmt.Println(err)
		jc.SendString(fmt.Sprintln(err))
		return
	}

	//this is a list of formats found for the video from youtube
	formatsFound := vid.Formats

	//lets just find all the 720p videos
	search := []string{res}

	//Filter requires a [] interface{}
	s := make([]interface{}, len(search))
	for i, v := range search {
		s[i] = v
	}

	formats := formatsFound.Filter(ytdl.FormatResolutionKey, s)

	//here i just pick one of the formats from the returned filtered list, could probably filter again if needed
	if len(formats) < 1 {
		jc.SendString("Invalid URL!")
		return
	}

	// remove unusal characters
	reg, err := regexp.Compile("/")
	if err != nil {
		fmt.Println(err)
		return
	}
	ptitle := reg.ReplaceAllString(vid.Title, "")

	///
	var fpath string
	fpath = fmt.Sprint(dir, ptitle, ".mp4")

	go down(url, formats[0], fpath)
}
func down(url string, format ytdl.Format, fpath string) {
	jc.SendString(fmt.Sprintln("Downloading ", url, " => ", fpath))
	file, err := os.Create(fpath)
	if err != nil {
		fmt.Println(err)
		jc.SendString(fmt.Sprintln(err))
		return
	}
	defer file.Close()

	vid, err := ytdl.GetVideoInfo(url)
	if err != nil {
		fmt.Println(err)
		jc.SendString(fmt.Sprintln(err))
		return
	}

	err = vid.Download(format, file)
	//u, err := vid.GetDownloadURL(formats[0])
	if err != nil {
		fmt.Println(err)
		jc.SendString(fmt.Sprintln(err))
		return
	}
	jc.SendString("Download completed successfully!")
}
