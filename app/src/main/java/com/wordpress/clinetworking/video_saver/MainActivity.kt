package com.wordpress.clinetworking.video_saver


import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.widget.Toast
import android.widget.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import gomobile.Gomobile
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.android.UI
import android.text.method.ScrollingMovementMethod
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import org.jetbrains.anko.alert
import org.jetbrains.anko.longToast
import org.jetbrains.anko.selector
import java.io.File


class MainActivity : AppCompatActivity() {
    var extStore = Environment.getExternalStorageDirectory();
    var w = extStore.toString() + "/vid-saver/"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // handle us being opened by someone
        // Get intent, action and MIME type
        val intent = intent
        val action = intent.action
        val data = intent.data
        val type = intent.type

        if (Intent.ACTION_SEND == action && type != null && intent.hasExtra(Intent.EXTRA_TEXT)) {
            if ("text/plain" == type) {
                var url = intent.getStringExtra(Intent.EXTRA_TEXT)
                launch(UI) { Download(url, w) }// Handle text being sent
            }
        }

        // scrollview
        val tview: TextView = findViewById(R.id.textView)
        val smm = ScrollingMovementMethod()
        tview.movementMethod = smm

        // callback
        val gocb = GoCallback(this, CommonPool)
        Gomobile.registerJavaCallback(gocb)

        //permissions
        val permissions = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        ActivityCompat.requestPermissions(this, permissions, 0)

        button.setOnClickListener {
            if (setupPermissions()) {
                launch(UI) { Download(editText.text.toString(), w) }// Handle text being sent
            } else {
                toast("Error insufficient permissions!")
            }

        }
    }

    fun Context.Download(url: String, dir: String) {
        val parent = File(dir)
        if (!parent.exists() && !parent.mkdirs()) {
            toast("Couldn't create dir for $w.")
            throw IllegalStateException("Couldn't create dir: " + parent);
        } else {
            val res = listOf("144p", "360p", "480p", "720p", "1080p")
            selector("Please select download type:", res, { dialogInterface, i ->
                toast("Downloading in background...")
                launch(CommonPool) { Gomobile.gogetvideo(url, dir, res[i]) }
            })
        }
    }


    private fun setupPermissions(): Boolean {
        val permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            toast("ERROR: Permission Denied!")
            return false
        } else {
            return true
        }
    }

    // creates toast messages to be displayed
    public fun Context.toast(message: CharSequence) =
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


}
