package com.wordpress.clinetworking.video_saver

import gomobile.JavaCallback
import android.app.Activity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.CommonPool


class GoCallback(internal var context: Activity, internal var commoncontext: CommonPool) : JavaCallback {

    override final fun sendString(data: String) {
        try {
            var d: CharSequence
            if (!data.contains("\n")) {
                d = data + "\n"
            } else {
                d = data
            }
            val i = data.length
            println("TXT: $data : $i")

            context.runOnUiThread(java.lang.Runnable {
                context.textView.append(d)
                Toast.makeText(context, d, Toast.LENGTH_SHORT).show()
                print(d)
            })
        } catch (e: Exception) {
            print("Error updating UI: ")
            println(e)
        }
    }
}